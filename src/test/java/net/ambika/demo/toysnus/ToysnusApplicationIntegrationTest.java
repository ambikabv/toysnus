package net.ambika.demo.toysnus;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


/**
 * @author Ambika
 * <p>
 * This integration test loads data from sql/ProdType.sql
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ToysnusApplication.class)
@ActiveProfiles("inttest")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ToysnusApplicationIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void a_shouldReturn27Products() throws Exception {
        mvc.perform(get("/product/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(27)));
    }

    @Test
    public void b_shouldCreateCartAddProductWithQuantityOne() throws Exception {
        mvc.perform(post("/cart/add/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalPrice").value("5.0"));
    }

    @Test
    public void c_shouldAddProductWithQuantity() throws Exception {
        mvc.perform(post("/cart/1/add/10/quantity/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalPrice").value("55.0"));
    }

    @Test
    public void d_shouldAddAnotherProductWithQuantity() throws Exception {
        mvc.perform(post("/cart/1/add/5/quantity/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalPrice").value("65.0"));
    }

    @Test
    public void e_shouldSearchProductInCart() throws Exception {
        mvc.perform(get("/cart/1/search/anna")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].product.productName", is("Anna")));
    }

    @Test
    public void f_shouldDeleteAProductInCart() throws Exception {
        mvc.perform(delete("/cart/1/product/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalPrice").value("10.0"));
    }

}
