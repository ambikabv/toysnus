package net.ambika.demo.toysnus.repository;

import net.ambika.demo.toysnus.data.TestData;
import net.ambika.demo.toysnus.model.Cart;
import net.ambika.demo.toysnus.model.CartItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


/**
 * @author Ambika
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CartRepositoryTest extends TestData {


    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CartRepository cartRepo;


    @Before
    public void setUp() throws Exception {
        super.setupData();
        entityManager.persist(toycar);
        entityManager.persist(plushToy);
        entityManager.persist(carrot);
        entityManager.persist(chair);

        CartItem item1 = new CartItem(toycar, 2L);
        CartItem item2 = new CartItem(plushToy, 5L);
        CartItem item3 = new CartItem(carrot, 1L);

        Cart testCart = new Cart();
        testCart.setCartItems(new HashSet<CartItem>() {{
            add(item1);
            add(item2);
            add(item3);
        }});

        entityManager.persist(testCart);
        entityManager.flush();
    }

    @Test
    public void shouldBeOnlyOneCart() {
        List<Cart> carts = cartRepo.findAll();
        assertThat(carts.size(), is(1));
    }


    @Test
    public void shouldHaveThreeItemsInCart() {
        List<Cart> carts = cartRepo.findAll();
        Cart onlyOneCart = carts.get(0);
        assertThat(onlyOneCart, is(notNullValue()));
        assertThat(onlyOneCart.getCartItems().size(), is(3));
    }


    @Test
    public void shouldFindMatchingCartItemInCart() {
        List<Cart> carts = cartRepo.findAll();
        Cart onlyOneCart = carts.get(0);
        assertThat(onlyOneCart, is(notNullValue()));
        List<CartItem> matchingCartItems = cartRepo.findCartItems(onlyOneCart.getId(), "mickey");
        assertThat(matchingCartItems.size(), is(1));
        assertThat(matchingCartItems.get(0).getProduct().getProductName(), is(plushToy.getProductName()));
    }


    @After
    public void cleanUp() {
        entityManager.clear();
    }
}
