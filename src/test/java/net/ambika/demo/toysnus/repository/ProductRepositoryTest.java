package net.ambika.demo.toysnus.repository;

import net.ambika.demo.toysnus.data.TestData;
import net.ambika.demo.toysnus.model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


/**
 * @author Ambika
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest extends TestData {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepo;


    @Before
    public void setUp() throws Exception {
        super.setupData();
        entityManager.persist(toycar);
        entityManager.persist(plushToy);
        entityManager.persist(carrot);
        entityManager.persist(chair);
        entityManager.flush();
    }

    @Test
    public void shouldFindByName_And_ReturnAMatchingProduct() {
        Optional<Product> foundProduct = productRepo.findOne(Example.of(toycar));

        assertTrue(foundProduct.isPresent());
        assertThat(foundProduct.get().getProductName(), is("Hotwheels tiny"));
    }

    @Test
    public void shouldRetrieveAllProducts() {
        List<Product> foundProduct = productRepo.findAll();
        assertThat(foundProduct.size(), is(4));
    }

    @After
    public void cleanUp() {
        entityManager.clear();
    }
}
