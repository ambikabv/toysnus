package net.ambika.demo.toysnus.data;

import net.ambika.demo.toysnus.model.Product;
import net.ambika.demo.toysnus.model.ProductType;
import org.mockito.Mockito;
import org.springframework.data.domain.Example;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Ambika
 */
public abstract class TestData {

    protected ProductType toy, food, furniture;
    protected Product toycar, plushToy, carrot, chair;


    protected void setupData() {
        toy = new ProductType("toy");
        food = new ProductType("food");
        furniture = new ProductType("furniture");

        toycar = new Product(toy, "Hotwheels tiny", 0.99, "item");
        plushToy = new Product(toy, "Mickey", 4.99, "item");
        carrot = new Product(food, "Carrots", 4.0, "lb");
        chair = new Product(furniture, "Chair", 29.99, "piece");

    }

    protected void setupDataWithId() {
        setupData();
        toy.setId(1L);
        food.setId(2L);
        furniture.setId(3L);

        toycar.setId(1L);
        plushToy.setId(2L);
        carrot.setId(3L);
        chair.setId(4L);
    }
}
