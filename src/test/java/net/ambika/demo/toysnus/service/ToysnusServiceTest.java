package net.ambika.demo.toysnus.service;


import net.ambika.demo.toysnus.config.ToysnusTestConfiguration;
import net.ambika.demo.toysnus.data.TestData;
import net.ambika.demo.toysnus.model.Product;
import net.ambika.demo.toysnus.repository.CartRepository;
import net.ambika.demo.toysnus.repository.ProductRepository;
import net.ambika.demo.toysnus.service.impl.ToysnusServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Example;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


/**
 * @author Ambika
 * <p>
 * Unit test for ToysnusService
 */
@RunWith(SpringRunner.class)
public class ToysnusServiceTest extends TestData {

    @TestConfiguration
    static class ToysnusServiceTestConfiguration {
        @Bean
        public ToysnusService toysnusService() {
            return new ToysnusServiceImpl();
        }
    }

    @Autowired
    private ToysnusService toysnusService;

    @MockBean
    private ProductRepository productRepo;

    @MockBean
    private CartRepository cartRepo;

    @Before
    public void setUp() {
        super.setupDataWithId();

        Mockito.when(productRepo.findOne(Example.of(toycar)))
                .thenReturn(Optional.of(toycar));
        Mockito.when(productRepo.findOne(Example.of(plushToy)))
                .thenReturn(Optional.of(plushToy));
        Mockito.when(productRepo.findAll())
                .thenReturn(Arrays.asList(toycar, plushToy, carrot, chair));

    }

    @Test
    public void shouldFindToyCarProduct() throws Exception {
        Long toyCarId = 1L;

        Optional<Product> testProduct = toysnusService.findOne(toyCarId);

        assertTrue(((Optional) testProduct).isPresent());
        assertThat(testProduct.get().getProductName(), is("Hotwheels tiny"));

    }

    @Test
    public void shouldFindAll() throws Exception {
        List<Product> products = toysnusService.findAll();
        assertTrue(((List) products).size() == 4);
    }

}
