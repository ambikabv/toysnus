package net.ambika.demo.toysnus.config;

import net.ambika.demo.toysnus.controller.CartController;
import net.ambika.demo.toysnus.controller.ProductController;
import net.ambika.demo.toysnus.service.ToysnusService;
import net.ambika.demo.toysnus.service.impl.ToysnusServiceImpl;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Ambika
 */

@Configuration
@Profile("inttest")
public class ToysnusTestConfiguration {

    //or just use this component scan
    // @ComponentScan(basePackageClasses = { CartController.class,ProductController.class })
    @Bean
    @Primary
    public ProductController productController() {
        return new ProductController();
    }

    @Bean
    public CartController cartController() {
        return new CartController();
    }

    @Bean
    @Primary
    public ToysnusService toysnusService() {
        return new ToysnusServiceImpl();
    }
}
