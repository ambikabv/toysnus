package net.ambika.demo.toysnus.controller;

import net.ambika.demo.toysnus.data.TestData;
import net.ambika.demo.toysnus.service.ToysnusService;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import java.util.Arrays;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


/**
 * @author Ambika
 *
 * ProductController unit test class
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest extends TestData {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ToysnusService service;

    @Before
    public void setUp() throws Exception {
        super.setupDataWithId();
    }

    @Test
    public void shouldBeEmptyInitially() throws Exception {
        mvc.perform(
                get("/product/all").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(0)));
    }

    @Test
    public void givenProducts_shouldGetAllProductsAsJson() throws Exception {
        when(service.findAll()).thenReturn(Arrays.asList(toycar, plushToy, carrot, chair));
        mvc.perform(
                get("/product/all").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(4)));
    }

    @Test
    public void givenProducts_whenSearchName_shouldReturnMatchingProducts() throws Exception {
        when(service.findAll("furn")).thenReturn(Arrays.asList(chair));
        mvc.perform(
                get("/product/type/furn").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(1)));
    }
}