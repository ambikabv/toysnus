package net.ambika.demo.toysnus.controller;

import net.ambika.demo.toysnus.data.TestData;
import net.ambika.demo.toysnus.dto.CartResource;
import net.ambika.demo.toysnus.model.Cart;
import net.ambika.demo.toysnus.model.CartItem;
import net.ambika.demo.toysnus.service.ToysnusService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;

import static org.mockito.Mockito.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Ambika
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CartController.class)
public class CartControllerTest extends TestData {

    @Autowired
    private MockMvc mvc;

    //Cart controller references ToysnusService, mock it
    @MockBean
    private ToysnusService service;

    @Before
    public void setUp() throws Exception {
        super.setupDataWithId();
    }

    @Test
    public void givenProducts_shouldCreateCartAndAddAProduct() throws Exception {
        CartItem item1 = new CartItem(toycar, 1L);
        item1.setId(1L);

        Cart testCartWithOneItem = new Cart();
        testCartWithOneItem.setCartItems(new HashSet<CartItem>() {{
            add(item1);
        }});
        testCartWithOneItem.setStatus("New");
        testCartWithOneItem.setTotalPrice(toycar.getPrice() * 1);
        testCartWithOneItem.setId(1L);

        CartResource testCartResource = new CartResource(testCartWithOneItem);
        when(service.addItem(null, 1L, 1L)).thenReturn(testCartResource);
        mvc.perform(
                post("/cart/add/{productId}", 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalPrice").value("0.99"));
    }
//	@Test
//	public void testNewCart() throws Exception {
//		testMVC.perform(
//				get("/cart")
//						.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(jsonPath("$.totalPrice").value("0"));
//	}

}
