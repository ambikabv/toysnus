package net.ambika.demo.toysnus;

import net.ambika.demo.toysnus.controller.CartController;
import net.ambika.demo.toysnus.controller.CartControllerTest;
import net.ambika.demo.toysnus.controller.ProductController;
import net.ambika.demo.toysnus.controller.ProductControllerTest;
import net.ambika.demo.toysnus.model.Product;
import net.ambika.demo.toysnus.model.ProductType;
import net.ambika.demo.toysnus.repository.CartRepositoryTest;
import net.ambika.demo.toysnus.repository.ProductRepositoryTest;
import net.ambika.demo.toysnus.service.ToysnusServiceTest;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Ambika
 * <p>
 * Integration tests
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProductRepositoryTest.class,
        CartRepositoryTest.class,
        ToysnusServiceTest.class,
        ProductControllerTest.class,
        CartControllerTest.class
})
@ActiveProfiles("unitTest")
public class ToysnusApplicationUnitTests {
    @Test
    public void contextLoads() {
    }
}
