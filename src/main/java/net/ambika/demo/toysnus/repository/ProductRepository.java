package net.ambika.demo.toysnus.repository;

import net.ambika.demo.toysnus.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Ambika
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
