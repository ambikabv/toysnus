package net.ambika.demo.toysnus.repository;

import net.ambika.demo.toysnus.model.Cart;
import net.ambika.demo.toysnus.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Ambika
 *
 * Cart JPA repository
 */
@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query(value = "Select item from Cart c join CartItem item on c.id = item.cartId join Product prod\n" +
            "on item.product = prod.id\n" +
            "where c.id = ?1\n" +
            "and lower(prod.productName) like %?2%")
    List<CartItem> findCartItems(Long cartId, String search);

}
