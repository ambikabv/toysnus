package net.ambika.demo.toysnus.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.ambika.demo.toysnus.model.Product;
import org.springframework.data.web.JsonPath;
import org.springframework.hateoas.ResourceSupport;

/**
 * @author Ambika
 * <p>
 * Product DTO object based on hateoas
 */
public class ProductResource extends ResourceSupport {
    @JsonProperty
    public long id;
    public String name;
    public double price;
    public String unit;
    public String type;

    public ProductResource(Product productModel) {
        id = productModel.getId();
        name = productModel.getProductName();
        price = productModel.getPrice();
        unit = productModel.getUnit();
        if (productModel.getProductType() != null) {
            type = productModel.getProductType().getTypeName();
        }
    }
}
