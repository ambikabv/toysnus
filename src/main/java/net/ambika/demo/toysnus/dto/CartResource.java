package net.ambika.demo.toysnus.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.ambika.demo.toysnus.model.Cart;
import net.ambika.demo.toysnus.model.CartItem;
import org.springframework.hateoas.ResourceSupport;

import java.util.Set;

/**
 * @author Ambika
 * <p>
 * DTO object based on hateoas
 */
public class CartResource extends ResourceSupport {
    @JsonProperty
    public long id;
    public String status;
    public double totalPrice;
    public Set<CartItem> items;

    public CartResource(Cart cartModel) {
        id = cartModel.getId();
        status = cartModel.getStatus();
        totalPrice = cartModel.getTotalPrice();
        items = cartModel.getCartItems();
    }
}
