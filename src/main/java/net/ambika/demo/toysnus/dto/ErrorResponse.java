package net.ambika.demo.toysnus.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ambika
 */
public class ErrorResponse {

    @JsonProperty
    private int status;
    private String message;

    public ErrorResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
