package net.ambika.demo.toysnus.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CartItem")
public class CartItem extends AbstractModel implements Serializable {


    @Column(name = "cart_id")
    private Long cartId;

    @Column(name = "quantity")
    private Long quantity;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;

    public CartItem() {

    }

    public CartItem(Product product, Long quantity) {
        this.quantity = quantity;
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public int hashCode() {
        return getProduct().getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        CartItem notherItem = (CartItem) obj;
        return notherItem.getProduct().getId() == this.getProduct().getId();
    }
}
