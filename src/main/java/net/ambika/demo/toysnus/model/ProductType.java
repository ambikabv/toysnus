package net.ambika.demo.toysnus.model;


import javax.persistence.*;
import java.io.Serializable;

/***
 * @author Ambika
 *
 * Product type model
 */
@Entity
@Table(name = "ProductType")
public class ProductType extends AbstractModel implements Serializable {

    @Column(name = "name")
    private String typeName;

    public ProductType() {
    }

    public ProductType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
