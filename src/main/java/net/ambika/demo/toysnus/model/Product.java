package net.ambika.demo.toysnus.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Product")

public class Product extends AbstractModel implements Serializable {

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "product_type")
    private ProductType productType;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_price")
    private Double price;

    @Column(name = "product_unit")
    private String unit;

    public Product() {
    }

    public Product(ProductType productType, String productName, Double price, String unit) {
        this.productType = productType;
        this.productName = productName;
        this.price = price;
        this.unit = unit;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
