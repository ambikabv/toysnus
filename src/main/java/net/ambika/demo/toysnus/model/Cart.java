package net.ambika.demo.toysnus.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Ambika
 * <p>
 * Cart model. contains cart items
 */
@Entity
@Table(name = "Cart")
public class Cart extends AbstractModel implements Serializable {

    @Column(name = "status")
    private String status;

    @Column(name = "total_price")
    private Double totalPrice;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "cart_id")
    private Set<CartItem> cartItems;

    public Cart() {
    }
//    public Cart(String status, Double totalPrice) {
//        this.status = status;
//        this.totalPrice = totalPrice;
//    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Set<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Set<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

}
