package net.ambika.demo.toysnus.exception;

/**
 * @author Ambika
 */
public class GenericBadRequest extends Exception {
    public GenericBadRequest(String message) {
        super(message);
    }
}
