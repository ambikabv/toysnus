package net.ambika.demo.toysnus.exception;

/**
 * @author Ambika
 * <p>
 * Exception when search results are empty
 */
public class NoProductFound extends Exception {

    private static final String MSG = "No products found";

    public NoProductFound() {
        super(MSG);
    }
}
