package net.ambika.demo.toysnus.exception;

import net.ambika.demo.toysnus.dto.ErrorResponse;
import net.ambika.demo.toysnus.exception.NoProductFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.http.HttpStatus;

/**
 * @author Ambika
 * <p>
 * A global exceptional handler and rest response translator
 */
@RestControllerAdvice
public class ToysnusExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ToysnusExceptionHandler.class);

    @ExceptionHandler(value = {GenericBadRequest.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse invalidRequest(GenericBadRequest ex) {
        return new ErrorResponse(400, ex.getMessage());
    }


    @ExceptionHandler(value = {InvalidProductQuantity.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse invalidProdQuantity(InvalidProductQuantity ex) {
        return new ErrorResponse(400, ex.getMessage());
    }

    @ExceptionHandler(value = {NoProductFound.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse noProductFound(NoProductFound ex) {
        return new ErrorResponse(404, ex.getMessage());
    }


    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse internalServerError(Exception ex) {
        LOG.error("Server Error", ex);
        return new ErrorResponse(500, ex.getMessage());
    }
}
