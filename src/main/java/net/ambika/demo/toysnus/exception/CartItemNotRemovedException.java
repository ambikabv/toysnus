package net.ambika.demo.toysnus.exception;

/**
 * @author Ambika
 */
public class CartItemNotRemovedException extends Exception {
    private static final String MSG = "CartItem not removed.";

    public CartItemNotRemovedException() {
        super(MSG);
    }
}
