package net.ambika.demo.toysnus.exception;

/**
 * @author Ambika
 */
public class InvalidProductQuantity extends Exception {
    private static final String MSG = "Invalid product quantity. quantity should be greater than zero";

    public InvalidProductQuantity() {
        super(MSG);
    }
}
