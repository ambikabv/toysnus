package net.ambika.demo.toysnus.log;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Ambika
 *
 * Generic aspect that logs any restcontroller access
 */
@Aspect
@Component
public class LoggerAspect {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Before("execution(public * net.ambika.demo.toysnus.controller.*Controller.*(..))")
    public void logBeforeRestControllers(JoinPoint jointpoint) throws Throwable {
        LOG.info("REST API call made to {}.{} with arguments",
                jointpoint.getTarget().getClass().getName(),
                jointpoint.getSignature().getName(),
                jointpoint.getArgs());
        //jointpoint.proceed();
    }
}
