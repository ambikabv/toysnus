package net.ambika.demo.toysnus.service.impl;

import net.ambika.demo.toysnus.dto.CartResource;
import net.ambika.demo.toysnus.exception.CartItemNotRemovedException;
import net.ambika.demo.toysnus.exception.GenericBadRequest;
import net.ambika.demo.toysnus.exception.InvalidProductQuantity;
import net.ambika.demo.toysnus.exception.NoProductFound;
import net.ambika.demo.toysnus.model.Cart;
import net.ambika.demo.toysnus.model.CartItem;
import net.ambika.demo.toysnus.model.Product;
import net.ambika.demo.toysnus.model.ProductType;
import net.ambika.demo.toysnus.repository.CartRepository;
import net.ambika.demo.toysnus.repository.ProductRepository;
import net.ambika.demo.toysnus.service.ToysnusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * @author Ambika
 */
@Service
public class ToysnusServiceImpl implements ToysnusService {

    private static final Logger LOG = LoggerFactory.getLogger(ToysnusServiceImpl.class);
    @Autowired
    ProductRepository productRepository;

    @Autowired
    CartRepository cartRepository;


    @Override
    public Optional<Product> findOne(Long id) {
        LOG.info("searching product with id {}", id);
        Product sampleProduct = new Product();
        sampleProduct.setId(id);
        return productRepository.findOne(Example.of(sampleProduct));
    }


    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> findAll(String productType) {
        Product sampleProduct = new Product();
        ProductType sampleType = new ProductType(productType);
        sampleProduct.setProductType(sampleType);

        //caseInsensitive, regex matcher
        ExampleMatcher caseInSensitiveMatcher =
                ExampleMatcher.matching()
                        .withIgnoreCase()
                        .withMatcher("productType.typeName", match -> match.contains());
        return productRepository.findAll(Example.of(sampleProduct, caseInSensitiveMatcher));
    }

    @Override
    public Optional<Cart> getCart(Long cartID) {
        return cartRepository.findById(cartID);
    }
    @Override
    public Cart createCart() {
        Cart newCart = new Cart();
        newCart.setStatus("new");
        newCart.setTotalPrice(0.0D);
        return cartRepository.save(newCart);
    }

    @Override
    public CartResource addItem(final Long cartId,
                                final Long productId,
                                final Long quantity)
            throws NoProductFound, GenericBadRequest, InvalidProductQuantity {
        //TODO in future - params can be validated using Spring validators
        if (quantity <= 0) {
            throw new InvalidProductQuantity();
        }
        //First make sure we find a matching product
        Optional<Product> product = findOne(productId);

        if (!product.isPresent())
            throw new NoProductFound();
        //product found, create cartItem
        CartItem cartItem = new CartItem(product.get(), quantity);

        //TODO need to support transaction capabilities either through synchronization
        // or through database locks

        // /product found, find matching cart, if not create one
        Optional<Cart> optionalCart = cartId == null ? Optional.empty() : cartRepository.findById(cartId);
        //if no cart found, create one;
        final Cart cart;
        if (!optionalCart.isPresent()) {
            if (cartId == null) {
                cart = createCart();
            } else {
                throw new GenericBadRequest(String.format("Cart %d not found", cartId));
            }

        } else {
            cart = optionalCart.get();
        }
        //TODO make this as a Builder pattern. eg: CartBuilder.createCart().addItems().updatePrice()
        return new CartResource(cartRepository.save(updateCartPrice(addItem(cart, cartItem))));
    }

    /**
     * Adds cartitem to the cart. If exists, just increase the corresponding quantity
     *
     * @param cart        - Cart that carries cart items
     * @param newCartItem - a product with quantity
     * @return modified cart
     */
    private Cart addItem(Cart cart, CartItem newCartItem) {
        if (cart.getCartItems() == null) {
            cart.setCartItems(new HashSet<CartItem>() {{
                                  this.add(newCartItem);
                              }}
            );
            return cart;

        }
        boolean added = cart.getCartItems().add(newCartItem);

        if (!added) {
            cart.getCartItems().forEach(existingCartItem -> {
                if (existingCartItem.equals(newCartItem)) {
                    existingCartItem.setQuantity(existingCartItem.getQuantity() + newCartItem.getQuantity());
                }
            });
        }

        return cart;
    }

    /**
     * updates cart price
     *
     * @param cart
     * @return updated cart (not necessary)
     */
    private Cart updateCartPrice(Cart cart) {

        double totalPrice =
                cart.getCartItems()
                        .stream()
                        .mapToDouble(cartItem -> cartItem.getQuantity() * cartItem.getProduct().getPrice())
                        .sum();
        LOG.debug("updating cart's {} price to {}", cart.getId(), totalPrice);
        cart.setTotalPrice(totalPrice);
        return cart;
    }

    /**
     * removes an item/product from cart, given cart id
     * @param cartId
     * @param itemOrProductId - it can be either CartItem Id or Product Id
     * @param isProductID - is above a product id
     * @return
     * @throws GenericBadRequest
     * @throws CartItemNotRemovedException
     */
    @Override
    public Cart deleteItem(
            final Long cartId,
            final Long itemOrProductId,
            boolean isProductID) throws GenericBadRequest, CartItemNotRemovedException {
        //find cart first
        Optional<Cart> optionalCart = cartRepository.findById(cartId);

        if (!optionalCart.isPresent())
            throw new GenericBadRequest(String.format("Cart %d not found", cartId));

        Cart cart = optionalCart.get();

        boolean hasRemoved = cart.getCartItems().removeIf(cartItem -> {
            if (isProductID) {
                return cartItem.getProduct().getId() == itemOrProductId;
            } else {
                return cartItem.getId() == itemOrProductId;
            }
        });
        if (!hasRemoved) {
            throw new CartItemNotRemovedException();
        }
        return cartRepository.save(updateCartPrice(cart));
    }

    @Override
    public List<CartItem> findItems(final Long id, final String search) throws GenericBadRequest {
        Optional<Cart> optionalCart = cartRepository.findById(id);
        if (!optionalCart.isPresent())
            throw new GenericBadRequest("Cart not found");
        //find call cartItems
        return cartRepository.findCartItems(id, search.toLowerCase());
    }


/*   public CartResource addItem(Long id, Long productId, Long quantity) throws NoProductFound, InvalidProductQuantity, GenericBadRequest {
        //TODO in future - params can be validated using Spring validators
        if (quantity <= 0) {
            throw new InvalidProductQuantity();
        }
        return new CartResource(addItem(id, productId, quantity));
    }*/

}
