package net.ambika.demo.toysnus.service;

import net.ambika.demo.toysnus.dto.CartResource;
import net.ambika.demo.toysnus.exception.CartItemNotRemovedException;
import net.ambika.demo.toysnus.exception.GenericBadRequest;
import net.ambika.demo.toysnus.exception.InvalidProductQuantity;
import net.ambika.demo.toysnus.exception.NoProductFound;
import net.ambika.demo.toysnus.model.Cart;
import net.ambika.demo.toysnus.model.CartItem;
import net.ambika.demo.toysnus.model.Product;

import java.util.List;
import java.util.Optional;

/**
 * @author Ambika
 */
public interface ToysnusService {

    Optional<Product> findOne(final Long id);

    List<Product> findAll();

    List<Product> findAll(final String productType);

    Cart createCart();

    CartResource addItem(final Long cartId, final Long productId, final Long quantity) throws NoProductFound, GenericBadRequest, InvalidProductQuantity;

    Optional<Cart> getCart(final Long cartID);

    Cart deleteItem(
            final Long cartId,
            final Long itemOrProductId,
            boolean isProductID) throws GenericBadRequest, CartItemNotRemovedException;

    List<CartItem> findItems(final Long id, final String search) throws GenericBadRequest;

}
