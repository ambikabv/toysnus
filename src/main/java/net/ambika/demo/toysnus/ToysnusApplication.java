package net.ambika.demo.toysnus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author Ambika
 *
 *
 * Springboot starter application - Toys n Us
 */

@SpringBootApplication
public class ToysnusApplication {

    private static final Logger LOG = LoggerFactory.getLogger(ToysnusApplication.class);

    public static void main(String[] args) {
        LOG.debug("Application toysnus starting...");
        SpringApplication.run(ToysnusApplication.class, args);
    }
}
