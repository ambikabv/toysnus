package net.ambika.demo.toysnus.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Ambika
 * <p>
 * Springboot configuration for toysnus, eg: jpa, repo etc
 */

@Configuration
@EnableJpaRepositories(basePackages = "net.ambika.demo.toysnus.repository")
@EntityScan(basePackages = "net.ambika.demo.toysnus.model")
@EnableJpaAuditing
public class ToysnusConfig {

}
