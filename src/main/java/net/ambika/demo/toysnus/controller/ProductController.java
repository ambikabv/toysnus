package net.ambika.demo.toysnus.controller;

import net.ambika.demo.toysnus.dto.ProductResource;
import net.ambika.demo.toysnus.exception.NoProductFound;
import net.ambika.demo.toysnus.model.Product;
import net.ambika.demo.toysnus.model.ProductType;
import net.ambika.demo.toysnus.service.ToysnusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
/**
 * @author Ambika
 * <p>
 * Restcontroller for Product related actions
 * currently supports listing all products, search by type
 * <p>
 * TODO support hateoas links
 */
@RestController
@RequestMapping("/product")
public class ProductController extends AbstractController {

    @Autowired
    private ToysnusService service;

    @GetMapping("/all")
    public List<ProductResource> all() {
        LOG.info("retrieving all products");
        List<Product> products = service.findAll();
        //create hateoas based response
        List<ProductResource> outProducts = products
                .stream()
                .map(product -> createHateoasProductResource(product))
                .collect(Collectors.toList());

        return outProducts;
    }


    @GetMapping("/{id}")
    public ProductResource get(@PathVariable long id) throws NoProductFound {
        LOG.info("search all products matching type {}", id);

        Optional<Product> result = service.findOne(id);
        if (result.isPresent()) {
            return createHateoasProductResource(result.get());
        } else {
            LOG.error("No products found for type {}", id);
            throw new NoProductFound();
        }
    }


    @GetMapping("/type/{type}")
    public List<Product> byType(@PathVariable String type) {
        return service.findAll(type);
    }

    private ProductResource createHateoasProductResource(Product product) {
        ProductResource productRes = new ProductResource(product);
        //productRes.add(createRestHateoasLink(product.getId()));
        productRes.add(linkTo(getClass()).slash(product.getId()).withSelfRel().withTitle("details"));
        productRes.add(linkTo(CartController.class).slash("add").slash(product.getId()).withRel("addToCart"));
        return productRes;
    }
}
