package net.ambika.demo.toysnus.controller;

import net.ambika.demo.toysnus.dto.CartResource;
import net.ambika.demo.toysnus.exception.CartItemNotRemovedException;
import net.ambika.demo.toysnus.exception.GenericBadRequest;
import net.ambika.demo.toysnus.exception.InvalidProductQuantity;
import net.ambika.demo.toysnus.exception.NoProductFound;
import net.ambika.demo.toysnus.model.Cart;
import net.ambika.demo.toysnus.model.CartItem;
import net.ambika.demo.toysnus.model.Product;
import net.ambika.demo.toysnus.service.ToysnusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author Ambika
 * RestController to manage cart. In typical implementation request mappings doesn't accept cart id in url.
 * recommended solution is to use cart object in request session (via cookie)
 *
 */
@RestController
@RequestMapping("/cart")
public class CartController extends AbstractController {

    @Autowired
    private ToysnusService service;


    /**
     * Creates a new cart and returns it
     *
     * @return Cart Resource
     */
    @GetMapping("")
    public CartResource cart() {
        //create cart using service and return translated cart resource
        //exception is handed via Toysnus ExceptionHandler
        //TODO in future use cookies rather than using rest response,
        // easy to access cart resources via cookie than passing via url as id
        return new CartResource(service.createCart());
    }

    /**
     * Creates a new Cart and adds the product to it.
     * @param productId
     * @return Cart Resource
     * @throws NoProductFound
     * @throws InvalidProductQuantity
     * @throws GenericBadRequest
     */
    @PostMapping("/add/{productId}")
    public CartResource addNew(@PathVariable long productId) throws NoProductFound, InvalidProductQuantity, GenericBadRequest {
        return service.addItem(null, productId, 1L);
    }

    /**
     * Adds the product to the given cart(default quantity 1)
     * @param id - Cart ID
     * @param productId - Product ID that needs to be added
     * @return Cart Resource
     * @throws NoProductFound
     * @throws InvalidProductQuantity
     * @throws GenericBadRequest
     */
    @PostMapping("/{id}/add/{productId}")
    public CartResource add(@PathVariable long id, @PathVariable long productId) throws NoProductFound, InvalidProductQuantity, GenericBadRequest {
        return service.addItem(id, productId, 1L);
    }

    /**
     * Adds the given quantity of given product to the specific cart
     * @param id - Cart ID
     * @param productId - Product ID
     * @param quantity - Quanity of product
     * @return Cart Resource
     * @throws NoProductFound
     * @throws InvalidProductQuantity
     * @throws GenericBadRequest
     */
    @PostMapping("/{id}/add/{productId}/quantity/{quantity}")
    public CartResource addWithQuantity(
            @PathVariable long id,
            @PathVariable long productId,
            @PathVariable long quantity) throws NoProductFound, InvalidProductQuantity, GenericBadRequest {
        //TODO in future - params can be validated using Spring validators
        return service.addItem(id, productId, quantity);
    }

    /**
     * searches item in cart.
     *
     * @param id   - cart id
     * @param name - product name
     * @return List of CartItems found in the cart
     */
    @GetMapping("/{id}/search/{name}")
    public List<CartItem> searchItem(@PathVariable Long id, @PathVariable String name) throws GenericBadRequest {
        if (null == id) {
            throw new GenericBadRequest("Cart not found");
        }
        if (name == null || name.trim().length() == 0) {
            throw new GenericBadRequest("Search string cannot be empty");
        }
        return service.findItems(id, name);
    }

    @DeleteMapping("/{id}/product/{productId}")
    public Cart deleteItemByProductId(
            @PathVariable Long id,
            @PathVariable Long productId) throws GenericBadRequest, CartItemNotRemovedException {
        if (null == id) {
            throw new GenericBadRequest("Cart not found");
        }
        //TODO add more validators
        return service.deleteItem(id, productId, true);
    }

    /**
     * deletes the item/product from the cart.
     * Error if no cart found
     * @param id - cart ID
     * @param itemId - product ID
     * @return Cart
     * @throws GenericBadRequest
     * @throws CartItemNotRemovedException
     */
    @DeleteMapping("/{id}/item/{itemId}")
    public Cart deleteItem(
            @PathVariable Long id,
            @PathVariable Long itemId) throws GenericBadRequest, CartItemNotRemovedException {
        if (null == id) {
            throw new GenericBadRequest("Cart not found");
        }
        //TODO add more validators
        return service.deleteItem(id, itemId, false);
    }

}
