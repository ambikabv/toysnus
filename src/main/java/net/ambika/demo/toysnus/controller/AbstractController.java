package net.ambika.demo.toysnus.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ambika
 */
public class AbstractController {
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

}
