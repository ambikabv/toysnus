	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(1, 'Disney Cars',sysdate,sysdate);
	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(2, 'PJ Masks',sysdate,sysdate);
	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(3, 'Disney Princess',sysdate,sysdate);
	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(4, 'LOL Surprise',sysdate,sysdate);
	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(5, 'SpongeBOB',sysdate,sysdate);
	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(6, 'Umi Team',sysdate,sysdate);
	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(7, 'Dora',sysdate,sysdate);
	INSERT INTO PRODUCT_TYPE(id,name,created_date,last_modified_date) VALUES(8, 'Hot Wheels',sysdate,sysdate);
	
	
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date) 
	VALUES(1, 'McQueen',1,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(2, 'Metor',1,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date) 
	VALUES(3, 'Luigi',1,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(4, 'Guido',1,5.00,'ITEM',sysdate,sysdate);
	
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date) 
	VALUES(5, 'Catboy',2,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date) 
	VALUES(6, 'Owlette',2,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(7, 'Gekko',2,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date) 
	VALUES(8, 'Birdie',2,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(9, 'Elsa',3,.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(10, 'Anna',3,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(11, 'Snow White',3,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(12, 'Rapunzel',3,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(13, 'LOL Series1',4,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(14, 'LOL Series2',4,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(15, 'LOL Confetti Pop',4,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(16, 'LOL Pets',4,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(17, 'LOL Lil Sisters',4,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(18, 'LOL Charm Fizz',4,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(19, 'Spongebob',5,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(20, 'Patrick',5,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(21, 'Squidward',5,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(22, 'Geo',6,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(23, 'Milli',6,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(24, 'Bot',6,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(25, 'Dora',7,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(26, 'Boots',7,5.00,'ITEM',sysdate,sysdate);
	INSERT INTO PRODUCT(id,product_name,product_type,product_price,product_unit,created_date,last_modified_date)
	VALUES(27, 'Ultimate Garage',8,5.00,'ITEM',sysdate,sysdate);
	