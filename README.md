### Toys n Us - a demo springboot shopping cart application
Demonstrates spring-boot, rest, hateoas, jpa, mockito, h2-db, cloudfoundry capabilities.

#### Main functions included are,

* Retrieve a list of products
    * <code>GET /product/all</code>
* Return list of products by type
    * <code>GET /product/type/{search}</code>
* Add an item to a cart with quantity
    * <code>POST /cart/add/{productId}</code>
* Delete an item
    * <code>DELETE /cart/{cartId}/{productId}</code>
* Search for an item
    * <code>GET /cart/{cartID}/search/{search} </code>



#### To run api server
<code>mvn clean spring-boot:run -Dspring.profiles.active=dev</code>
    
#### To run unit tests
<code>mvn clean spring-boot:run -Dspring.profiles.active=test</code>
    
#### To run integration test 
<code>mvn clean spring-boot:run -Dspring.profiles.active=inttest</code>
    

#### Cloudfoundry deployment
<code>cf push toysnus -p target/toysnus-<version>.jar</code>

Env var

<code>spring.profiles.active=demo</code>